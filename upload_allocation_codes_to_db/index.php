<?php
require_once('../inc/connection.php');

$csv = 'allocations.csv';

// Add Student Records
$load_query  = "LOAD DATA LOCAL INFILE '".$csv."' ";
$load_query .= "INTO TABLE allocations ";
$load_query .= "FIELDS TERMINATED BY ',' ";
$load_query .= "LINES TERMINATED BY '\r' ";
$load_query .= "(allocation,foundation_name,web_name);";
$load_result = mysqli_query($connection,$load_query);

if(!$load_result) {
	die(mysqli_error($connection));
}

mysqli_close($connection);

echo 'success';

?>