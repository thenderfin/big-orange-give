<?php
    // Allow from any origin
    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
        header('Content-Type: application/json');
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }
?>
<?php

require_once('../connection.php');

$totals = array(
    "tot" => 0,
    "agr" => 0,
    "arc" => 0,
    "art" => 0,
    "ath" => 0,
    "bnd" => 0,
    "bus" => 0,
    "cam" => 0,
    "cci" => 0,
    "edu" => 0,
    "eng" => 0,
    "law" => 0,
    "lib" => 0,
    "mcm" => 0,
    "nur" => 0,
    "par" => 0,
    "soc" => 0,
    "gdn" => 0,
    "vet" => 0,
    "wuo" => 0,
    "large_AlphaChiOmega" => 0,
    "large_AlphaDeltaPi" => 0,
    "large_AlphaGammaRho" => 0,
    "large_AlphaOmicronPi" => 0,
    "large_AlphaTauOmega" => 0,
    "large_BetaUpsilonChi" => 0,
    "large_ChiOmega" => 0,
    "large_DeltaDeltaDelta" => 0,
    "large_DeltaGamma" => 0,
    "large_DeltaTauDelta" => 0,
    "large_DeltaZeta" => 0,
    "large_KappaAlpha" => 0,
    "large_KappaDelta" => 0,
    "large_KappaKappaGamma" => 0,
    "large_KappaSigma" => 0,
    "large_PhiMu" => 0,
    "large_PiBetaPhi" => 0,
    "large_PiKappaAlpha" => 0,
    "large_SigmaAlphaEpsilon" => 0,
    "large_SigmaKappa" => 0,
    "large_SigmaNu" => 0,
    "large_ZetaTauAlpha" => 0,
    "small_AlphaEpsilonPi" => 0,
    "small_AlphaKappaAlpha" => 0,
    "small_BetaChiTheta" => 0,
    "small_BetaThetaPi" => 0,
    "small_ChiPhi" => 0,
    "small_DeltaKappaEpsilon" => 0,
    "small_DeltaPhiOmega" => 0,
    "small_DeltaSigmaTheta" => 0,
    "small_Farmhouse" => 0,
    "small_IotaPhiTheta" => 0,
    "small_KappaAlphaPsi" => 0,
    "small_LambdaThetaAlpha" => 0,
    "small_OmegaPsiPhi" => 0,
    "small_PhiBetaSigma" => 0,
    "small_PhiDeltaTheta" => 0,
    "small_PhiGammaDelta" => 0,
    "small_PhiKappaPsi" => 0,
    "small_PiKappaPhi" => 0,
    "small_SigmaBetaRho" => 0,
    "small_SigmaGammaRho" => 0,
    "small_SigmaPhiEpsilon" => 0,
    "small_SigmaSigmaRho" => 0,
    "small_ZetaPhiBeta" => 0
);

// Get Goals
$goals_select_query  = "SELECT * FROM bog_goals WHERE id = 1 ";
$goals_select_result = mysqli_query($connection, $goals_select_query);
if(!$goals_select_result) {
    die("Goals select error: ".mysqli_error($connection));
}
$goals_array = mysqli_fetch_assoc($goals_select_result);
mysqli_free_result($goals_select_result);


// Get Totals
$totals_select_query = "SELECT college as 'group', COUNT(*) as count, SUM(amount) as total FROM `bog2018` GROUP BY college ";
$totals_select_query .= "UNION SELECT match_code as 'group', COUNT(*) as count, SUM(match_amount) as total FROM `bog2018` GROUP BY match_code;";
$totals_select_result = mysqli_query($connection, $totals_select_query);
if(!$totals_select_result) {
    die("Totals select error: ".mysqli_error($connection));
}
while($totals_row = mysqli_fetch_assoc($totals_select_result)) {
    $group = $totals_row['group'];
    $count = $totals_row['count'];
    $total = $totals_row['total'];

    // If greek, add count to group total and add total to overall
    if( (strpos($group, 'small') !== false) || (strpos($group, 'large') !== false) ) {
        $totals[$group] += $count;
        $totals['tot'] += $total;

    // If match, remove '_match' and add $total to corresponding total and overall
    } elseif (strpos($group, 'match') !== false) {
        $matched_group = str_replace('_match', '', $group);
        $totals['tot'] += $total;
        if( isset($totals[$matched_group]) ) {
            $totals[$matched_group] += $total;
        }

    // Otherwise add to overall and group total
    } else {
        $totals['tot'] += $total;
        if( isset($totals[$group]) ) {
            $totals[$group] += $total;
        }
    }
}
mysqli_free_result($totals_select_result);


// Get Manual Transactions
$select_query  = "SELECT allocation, COUNT(*) as count, sum(amount) as total, add_to_total FROM bog_juke  WHERE add_to_total = 1 GROUP BY allocation ";
$select_query .= "UNION SELECT allocation, COUNT(*) as count, sum(amount) as total, add_to_total FROM bog_juke WHERE add_to_total = 0 GROUP BY allocation;";
$select_result = mysqli_query($connection, $select_query);
if(!$select_result) {
    die("Juke select error: ".mysqli_error($connection));
}
while($select_row = mysqli_fetch_assoc($select_result)) {
    $group = $select_row['allocation'];
    $total = $select_row['total'];
    $add_to_total = $select_row['add_to_total'];
    // If greek, add total to group total
    if( (strpos($group, 'small') !== false) || (strpos($group, 'large') !== false) ) {
        $totals[$group] += $total;

    // If $add_to_total, add to overall total. Add to group total
    } else {
        if($add_to_total == 1) {
            $totals['tot'] += $total;
        }
        if( isset($totals[$group]) ) {
            $totals[$group] += $total;
        }
    }
}
mysqli_free_result($select_result);

// Close connection
mysqli_close($connection);

echo "{
		\"overall\": {
			\"goal\": {$goals_array['tot']},
			\"amount\": {$totals['tot']}
		},
		\"colleges\": {
			\"agr\": {
				\"goal\": {$goals_array['agr']},
				\"amount\": {$totals['agr']}
			},
			\"arc\": {
				\"goal\": {$goals_array['arc']},
				\"amount\": {$totals['arc']}
			},  
            \"art\": {
				\"goal\": {$goals_array['art']},
				\"amount\": {$totals['art']}
			}, 
            \"ath\": {
				\"goal\": {$goals_array['ath']},
				\"amount\": {$totals['ath']}
			}, 
            \"bnd\": {
				\"goal\": {$goals_array['bnd']},
				\"amount\": {$totals['bnd']}
			}, 
            \"bus\": {
				\"goal\": {$goals_array['bus']},
				\"amount\": {$totals['bus']}
			}, 
            \"cci\": {
				\"goal\": {$goals_array['cci']},
				\"amount\": {$totals['cci']}
			}, 
            \"edu\": {
				\"goal\": {$goals_array['edu']},
				\"amount\": {$totals['edu']}
			}, 
            \"eng\": {
				\"goal\": {$goals_array['eng']},
				\"amount\": {$totals['eng']}
			}, 
            \"law\": {
				\"goal\": {$goals_array['law']},
				\"amount\": {$totals['law']}
			}, 
            \"lib\": {
				\"goal\": {$goals_array['lib']},
				\"amount\": {$totals['lib']}
			}, 
            \"mcm\": {
				\"goal\": {$goals_array['mcm']},
				\"amount\": {$totals['mcm']}
			}, 
            \"nur\": {
				\"goal\": {$goals_array['nur']},
				\"amount\": {$totals['nur']}
			}, 
            \"par\": {
				\"goal\": {$goals_array['par']},
				\"amount\": {$totals['par']}
			}, 
            \"soc\": {
				\"goal\": {$goals_array['soc']},
				\"amount\": {$totals['soc']}
			},
            \"gdn\": {
				\"goal\": {$goals_array['gdn']},
				\"amount\": {$totals['gdn']}
			}, 
            \"vet\": {
				\"goal\": {$goals_array['vet']},
				\"amount\": {$totals['vet']}
			}, 
            \"wuo\": {
				\"goal\": {$goals_array['wuo']},
				\"amount\": {$totals['wuo']}
			}
		},
		\"greek\": {
			\"small\": [
				{
					\"name\": \"Alpha Epsilon Pi\",
                    \"gifts\": {$totals['small_AlphaEpsilonPi']}
				},
                {
					\"name\": \"Alpha Kappa Alpha\",
                    \"gifts\": {$totals['small_AlphaKappaAlpha']}
				},
                {
					\"name\": \"Beta Chi Theta\",
                    \"gifts\": {$totals['small_BetaChiTheta']}
				},
                {
					\"name\": \"Beta Theta Pi\",
                    \"gifts\": {$totals['small_BetaThetaPi']}
				},
                {
					\"name\": \"Chi Phi\",
                    \"gifts\": {$totals['small_ChiPhi']}
				},
                {
					\"name\": \"Delta Kappa Epsilon\",
                    \"gifts\": {$totals['small_DeltaKappaEpsilon']}
				},
                {
					\"name\": \"Delta Phi Omega\",
                    \"gifts\": {$totals['small_DeltaPhiOmega']}
				},
                {
					\"name\": \"Delta Sigma Theta\",
                    \"gifts\": {$totals['small_DeltaSigmaTheta']}
				},
                {
					\"name\": \"Farmhouse\",
                    \"gifts\": {$totals['small_Farmhouse']}
				},
                {
					\"name\": \"Iota Phi Theta\",
                    \"gifts\": {$totals['small_IotaPhiTheta']}
				},
                {
					\"name\": \"Kappa Alpha Psi\",
                    \"gifts\": {$totals['small_KappaAlphaPsi']}
				},
                {
					\"name\": \"Lambda Theta Alpha\",
                    \"gifts\": {$totals['small_LambdaThetaAlpha']}
				},
                {
					\"name\": \"Omega Psi Phi\",
                    \"gifts\": {$totals['small_OmegaPsiPhi']}
				},
                {
					\"name\": \"Phi Beta Sigma\",
                    \"gifts\": {$totals['small_PhiBetaSigma']}
				},
                {
					\"name\": \"Phi Delta Theta\",
                    \"gifts\": {$totals['small_PhiDeltaTheta']}
				},
                {
					\"name\": \"Phi Gamma Delta\",
                    \"gifts\": {$totals['small_PhiGammaDelta']}
				},
                {
					\"name\": \"Phi Kappa Psi\",
                    \"gifts\": {$totals['small_PhiKappaPsi']}
				},
                {
					\"name\": \"Pi Kappa Phi\",
                    \"gifts\": {$totals['small_PiKappaPhi']}
				},
                {
					\"name\": \"Sigma Beta Rho\",
                    \"gifts\": {$totals['small_SigmaBetaRho']}
				},
                {
					\"name\": \"Sigma Gamma Rho\",
                    \"gifts\": {$totals['small_SigmaGammaRho']}
				},
                {
					\"name\": \"Sigma Phi Epsilon\",
                    \"gifts\": {$totals['small_SigmaPhiEpsilon']}
				},
                {
					\"name\": \"Sigma Sigma Rho\",
                    \"gifts\": {$totals['small_SigmaSigmaRho']}
				},
                {
					\"name\": \"Zeta Phi Beta\",
                    \"gifts\": {$totals['small_ZetaPhiBeta']}
				}
			],
			\"large\": [
                {
                    \"name\": \"Alpha Chi Omega\",
                    \"gifts\": {$totals['large_AlphaChiOmega']}
                },
                {
                    \"name\": \"Alpha Delta Pi\",
                    \"gifts\": {$totals['large_AlphaDeltaPi']}
                },
                {
                    \"name\": \"Alpha Gamma Rho\",
                    \"gifts\": {$totals['large_AlphaGammaRho']}
                },
                {
                    \"name\": \"Alpha Omicron Pi\",
                    \"gifts\": {$totals['large_AlphaOmicronPi']}
                },
                {
                    \"name\": \"Alpha Tau Omega\",
                    \"gifts\": {$totals['large_AlphaTauOmega']}
                },
                {
                    \"name\": \"Beta Upsilon Chi\",
                    \"gifts\": {$totals['large_BetaUpsilonChi']}
                },
                {
                    \"name\": \"Chi Omega\",
                    \"gifts\": {$totals['large_ChiOmega']}
                },
                {
                    \"name\": \"Delta Delta Delta\",
                    \"gifts\": {$totals['large_DeltaDeltaDelta']}
                },
                {
                    \"name\": \"Delta Gamma\",
                    \"gifts\": {$totals['large_DeltaGamma']}
                },
                {
                    \"name\": \"Delta Tau Delta\",
                    \"gifts\": {$totals['large_DeltaTauDelta']}
                },
                {
                    \"name\": \"Delta Zeta\",
                    \"gifts\": {$totals['large_DeltaZeta']}
                },
                {
                    \"name\": \"Kappa Alpha\",
                    \"gifts\": {$totals['large_KappaAlpha']}
                },
                {
                    \"name\": \"Kappa Delta\",
                    \"gifts\": {$totals['large_KappaDelta']}
                },
                {
                    \"name\": \"Kappa Kappa Gamma\",
                    \"gifts\": {$totals['large_KappaKappaGamma']}
                },
                {
                    \"name\": \"Kappa Sigma\",
                    \"gifts\": {$totals['large_KappaSigma']}
                },
                {
                    \"name\": \"Phi Mu\",
                    \"gifts\": {$totals['large_PhiMu']}
                },
                {
                    \"name\": \"Pi Beta Phi\",
                    \"gifts\": {$totals['large_PiBetaPhi']}
                },
                {
                    \"name\": \"Pi Kappa Alpha\",
                    \"gifts\": {$totals['large_PiKappaAlpha']}
                },
                {
                    \"name\": \"Sigma Alpha Epsilon\",
                    \"gifts\": {$totals['large_SigmaAlphaEpsilon']}
                },
                {
                    \"name\": \"Sigma Kappa\",
                    \"gifts\": {$totals['large_SigmaKappa']}
                },
                {
                    \"name\": \"Sigma Nu\",
                    \"gifts\": {$totals['large_SigmaNu']}
                },
                {
                    \"name\": \"Zeta Tau Alpha\",
                    \"gifts\": {$totals['large_ZetaTauAlpha']}
                }
			]
		}
	}";

?>