<?php

function sort_response($response) {
	$response_string = json_encode($response);
	$clean_string = str_replace('@attributes', 'attributes', $response_string);
	$response_array = json_decode($clean_string, true);

	$all_transaction_array = array();
	if(isset($response_array['Transaction'][0])) {
		foreach($response_array['Transaction'] as $transaction) {
			$current_transaction_array = array();
			foreach($transaction['Column'] as $column) {
				$key = $column['attributes']['Name'];
				if($key == 'designations') {
					$designations = array();
					if(isset($column['Designations']['Designation']['attributes'])) {
						$amount = $column['Designations']['Designation']['attributes']['Amount'];
						$allocation_code = $column['Designations']['Designation']['Ids']['Id'][1]['attributes']['Value'];
						array_push($designations, array('amount'=>$amount,'allocation_code'=>$allocation_code)); 
					} else {
						foreach($column['Designations']['Designation'] as $designation) {
							$amount = $designation['attributes']['Amount'];
							$allocation_code = $designation['Ids']['Id'][1]['attributes']['Value'];
							array_push($designations, array('amount'=>$amount,'allocation_code'=>$allocation_code)); 
						}
					}
					$value = $designations;
				} else {
					$value = $column['attributes']['Value'];
				}
				$current_transaction_array[$key] = $value;
			}
			$all_transaction_array[] = $current_transaction_array;
		}
	} else {
		$transaction = $response_array['Transaction'];
		
		$current_transaction_array = array();
		foreach($transaction['Column'] as $column) {
			$key = $column['attributes']['Name'];
			if($key == 'designations') {
				$designations = array();
				if(isset($column['Designations']['Designation']['attributes'])) {
					$amount = $column['Designations']['Designation']['attributes']['Amount'];
					$allocation_code = $column['Designations']['Designation']['Ids']['Id'][1]['attributes']['Value'];
					array_push($designations, array('amount'=>$amount,'allocation_code'=>$allocation_code)); 
				} else {
					foreach($column['Designations']['Designation'] as $designation) {
						$amount = $designation['attributes']['Amount'];
						$allocation_code = $designation['Ids']['Id'][1]['attributes']['Value'];
						array_push($designations, array('amount'=>$amount,'allocation_code'=>$allocation_code)); 
					}
				}
				$value = $designations;
			} else {
				$value = $column['attributes']['Value'];
			}
			$current_transaction_array[$key] = $value;
		}
		$all_transaction_array[] = $current_transaction_array;
	}
		
	return $all_transaction_array;
}

function curl($url) {
	$curl = curl_init($url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	$response = simplexml_load_string(curl_exec($curl));
	curl_close($curl);

	return $response;
}

function sort_control_response($response) {
	$response_string = json_encode($response);
	$clean_string = str_replace('@attributes', 'attributes', $response_string);
	$response_array = json_decode($clean_string, true);
	$instance_array = array();

	if(isset($response_array['MemberInformation'])) {
		foreach($response_array['MemberInformation'] as $member_array) {
			foreach($member_array['Instances'] as $instances) {
				// If only a single Instance exists
				if(isset($instances['Column'])) {
					$instance_id = $instances['attributes']['Instance_Id'];
					foreach($instances['Column'] as $column) {
						if($column['attributes']['Name'] == 'utk source 20173') {
							$source = $column['attributes']['Value'];
						} elseif($column['attributes']['Name'] == 'utk-amt-20173') {
							$amount = $column['attributes']['Value'];
						}
					}
					$instance_array[$instance_id] = array(
						'source' => $source,
						'amount' => $amount
					);

				// If multiple Instances exist
				} else {
					foreach($instances as $instance) {
						$instance_id = $instance['attributes']['Instance_Id'];
						foreach($instance['Column'] as $column) {
							if($column['attributes']['Name'] == 'utk source 20173') {
								$source = $column['attributes']['Value'];
							} elseif($column['attributes']['Name'] == 'utk-amt-20173') {
								$amount = $column['attributes']['Value'];
							}
						}
						$instance_array[$instance_id] = array(
							'source' => $source,
							'amount' => $amount
						);
					}
				}
			}
		}
	}

	return $instance_array;
}

function get_allocation_map() {
	global $connection;
	$allocation_map = array();

	$select_query = "SELECT * FROM  allocations;";
	$select_result = mysqli_query($connection, $select_query);
	if(!$select_result) {
		// Save error, close connection, report error
		$error = mysqli_error($connection);
		mysqli_close($connection);
		error_log($error." on line ".__LINE__);
		die($error." on line ".__LINE__);
	}
	while($row = mysqli_fetch_assoc($select_result)) {
		$allocation_map[$row['allocation']] = $row['web_name'];
	}
	mysqli_free_result($select_result);

	return $allocation_map;
}

function determine_match($remaining_matches, $source_code, $amount, $college) {

	// Use College match first
	// If college match exhausted check each true match to see:
	//    - If current time is later than match start time
	//    - If funds exist in match

	// Decrease remainging match amount each time used
	// Return updated $remaining_matches array each time function is called

	$truMondayStart = date('U', strtotime('2018-11-12 5:00:00 a.m.'));
	$truTuesdayStart = date('U', strtotime('2018-11-13 5:00:00 a.m.')); // Only if source is emtuea, emtueb, or emtuec
	$truWednesdayOneStart = date('U', strtotime('2018-11-14 5:00:00 a.m.'));
	$truWednesdayTwoStart = date('U', strtotime('2018-11-14 6:00:00 p.m.'));
	$dobbsStart = date('U', strtotime('2018-11-14 12:00:01 a.m.'));
	$current_time = date('U');

	// Dobbs match
	// first $100 with matching source ( get codes from melanie ) total of $5000 - only wednesday higher priority than colleges
	// Only match if source is from specified email segment
	if( $current_time > $dobbsStart && ($source_code == 'jd1' || $source_code == 'jd2') && $remaining_matches['dob_match'] > 0 ) {
		$match_amount = min(100, $amount, $remaining_matches['dob_match']);
		$match_code = 'dob_match';
		$remaining_matches['dob_match'] -= $match_amount;
	
	// Match agr gifts 1:1 up to $100
	} elseif($college == 'agr' && $remaining_matches['agr_match'] > 0) {
		$match_amount = min(100, $amount, $remaining_matches['agr_match']);
		$match_code = 'agr_match';
		$remaining_matches['agr_match'] -= $match_amount;

	// Match bus gifts 1:1 
	} elseif ($college == 'bus' && $remaining_matches['bus_match'] > 0) {
		$match_amount = min($amount, $remaining_matches['bus_match']);
		$match_code = 'bus_match';
		$remaining_matches['bus_match'] -= $match_amount;

	// Match law gifts 2:1 up to $250
	} elseif ($college == 'law' && $remaining_matches['law_match'] > 0) {
		$match_amount = min(250, ($amount * 2), $remaining_matches['law_match']);
		$match_code = 'law_match';
		$remaining_matches['law_match'] -= $match_amount;

	// Match mcm gifts 2:1, no max
	} elseif ($college == 'mcm' && $remaining_matches['mcm_match'] > 0) {
		$match_amount = min(($amount * 2), $remaining_matches['mcm_match']);
		$match_code = 'mcm_match';
		$remaining_matches['mcm_match'] -= $match_amount;

	// Match eng gifts up to $100
	} elseif ($college == 'eng' && $remaining_matches['eng_match'] > 0) {
		$match_amount = min(100, $amount, $remaining_matches['eng_match']);
		$match_code = 'eng_match';
		$remaining_matches['eng_match'] -= $match_amount;

	// Match vet gifts 1:1 
	} elseif ($college == 'vet' && $remaining_matches['vet_match'] > 0) {
		$match_amount = min($amount, $remaining_matches['vet_match']);
		$match_code = 'vet_match';
		$remaining_matches['vet_match'] -= $match_amount;

	// Match mon gifts up to $100
	} elseif ($current_time > $truMondayStart && $remaining_matches['tru_match_mon'] > 0) {
		$match_amount = min(100, $amount, $remaining_matches['tru_match_mon']);
		$match_code = 'tru_match_mon';
		$remaining_matches['tru_match_mon'] -= $match_amount;

	// Match tue gifts up to $100
	} elseif ($current_time > $truTuesdayStart && $remaining_matches['tru_match_tue'] > 0 && ($source_code == 'emtuea' || $source_code == 'emtueb' || $source_code == 'emtuec') ) {
		// Only match if source is from specified email segment
		$match_amount = min(100, $amount, $remaining_matches['tru_match_tue']);
		$match_code = 'tru_match_tue';
		$remaining_matches['tru_match_tue'] -= $match_amount;

	// Match wed1 gifts up to $100
	} elseif ($current_time > $truWednesdayOneStart && $remaining_matches['tru_match_wed1'] > 0) {
		$match_amount = min(100, $amount, $remaining_matches['tru_match_wed1']);
		$match_code = 'tru_match_wed1';
		$remaining_matches['tru_match_wed1'] -= $match_amount;

	// Match wed2 gifts up to $100
	} elseif ($current_time > $truWednesdayTwoStart && $remaining_matches['tru_match_wed2'] > 0) {
		$match_amount = min(100, $amount, $remaining_matches['tru_match_wed2']);
		$match_code = 'tru_match_wed2';
		$remaining_matches['tru_match_wed2'] -= $match_amount;
	
	// If no matches apply or if all are used up
	} else {
		$match_amount = 0;
		$match_code = 'none_match';
	}

	$output = array(
		'amount' => $match_amount,
		'code' => $match_code,
		'remaining' => $remaining_matches
	);
	return $output;
}

function get_remaining_matches() {
	global $connection;

	// Get Total Match dollars for each match
	$select_query = "SELECT tru_match_mon,tru_match_tue,tru_match_wed1,tru_match_wed2,law_match,eng_match,agr_match,bus_match,mcm_match,vet_match,dob_match ";
	$select_query .= "FROM bog_goals WHERE id = 1;";
	$select_result = mysqli_query($connection, $select_query);
	if(!$select_result) {
		// Save error, close connection, report error
		$error = mysqli_error($connection);
		mysqli_close($connection);
		error_log($error." on line ".__LINE__);
		die($error." on line ".__LINE__);
	}
	$goals_array = mysqli_fetch_assoc($select_result);
	mysqli_free_result($select_result);

	// Get summed totals for all existing matches in db
	$bog_select_query = "SELECT match_code, sum(match_amount) as 'match_total' FROM bog2018 GROUP BY match_code;";
	$bog_select_result = mysqli_query($connection, $bog_select_query);
	if(!$bog_select_result) {
		// Save error, close connection, report error
		$error = mysqli_error($connection);
		mysqli_close($connection);
		error_log($error." on line ".__LINE__);
		die($error." on line ".__LINE__);
	}
	while($row = mysqli_fetch_assoc($bog_select_result)) {
		$match_code = $row['match_code'];
		$match_total = $row['match_total'];
		if( isset($goals_array[$match_code]) ) {
			$goals_array[$match_code] -= $match_total;
		}
	}
	mysqli_free_result($bog_select_result);

	return $goals_array;
}

function create_insert_strings ($transactions, $control_instances) {

	$allocation_map = get_allocation_map();
	$remaining_matches = get_remaining_matches();
	$insert_strings = array();

	foreach($transactions as $control => $array) {
		if(count($array) > 0) {
			foreach($array as $transaction) {
				$trans_id = $transaction['instance_id'];
				$time = $transaction['purchasedate'];
				$name = $transaction['billingname'];
				$email = $transaction['billingemail'];
				$payment_type = $transaction['paymenttype'];
				$reported = 0;
				$address = $transaction['billingstreet1'] . ' ' . $transaction['billingstreet2'] . ' ' . $transaction['billingcity'] . ', ' . $transaction['billingstate'] . ' ' . $transaction['billingzip'];
				$member_id = $transaction['constituent_id'];
				$control = $control;
				$source_code = isset( $control_instances[$trans_id] ) ? $control_instances[$trans_id]['source'] : '';

				if ( !isset($transaction['designations']) ) {
					$amount = $transaction['amount'];
					$allocation = 'none';
					$college = 'cam';
					$match = determine_match($remaining_matches, $source_code, $amount, $college);
					$remaining_matches = $match['remaining']; // Make sure new $remaining_matches is used each loop
					$match_amount = $match['amount'];
					$match_code = $match['code'];

					if(!isset($insert_strings['trans'])) {
						$insert_strings['trans'] = "(
							'{$trans_id}', 
							{$amount}, 
							'{$college}', 
							'{$time}', 
							'{$name}', 
							'{$email}', 
							'{$payment_type}', 
							{$reported}, 
							'{$address}', 
							'{$member_id}', 
							'{$allocation}', 
							{$match_amount}, 
							'{$match_code}',
							'{$control}'
						)";
					} else {
						$insert_strings['trans'] .= ",(
							'{$trans_id}', 
							{$amount}, 
							'{$college}', 
							'{$time}', 
							'{$name}', 
							'{$email}', 
							'{$payment_type}', 
							{$reported}, 
							'{$address}', 
							'{$member_id}', 
							'{$allocation}', 
							{$match_amount}, 
							'{$match_code}',
							'{$control}'
						)";
					}
				} else {
					foreach($transaction['designations'] as $designation) {
						$amount = $designation['amount'];
						$allocation = $designation['allocation_code'];
						$college = isset( $allocation_map[$allocation] ) ? $allocation_map[$allocation] : 'none';
						$match = determine_match($remaining_matches, $source_code, $amount, $college);
						$remaining_matches = $match['remaining']; // Make sure new $remaining_matches is used each loop
						$match_amount = $match['amount'];
						$match_code = $match['code'];

						if(!isset($insert_strings['trans'])) {
							$insert_strings['trans'] = "(
								'{$trans_id}', 
								{$amount}, 
								'{$college}', 
								'{$time}', 
								'{$name}', 
								'{$email}', 
								'{$payment_type}', 
								{$reported}, 
								'{$address}', 
								'{$member_id}', 
								'{$allocation}', 
								{$match_amount}, 
								'{$match_code}',
								'{$control}'
							)";
						} else {
							$insert_strings['trans'] .= ",(
								'{$trans_id}', 
								{$amount}, 
								'{$college}', 
								'{$time}', 
								'{$name}', 
								'{$email}', 
								'{$payment_type}', 
								{$reported}, 
								'{$address}', 
								'{$member_id}', 
								'{$allocation}', 
								{$match_amount}, 
								'{$match_code}',
								'{$control}'
							)";
						}
					}
				}
			}
		}
	}

	foreach($control_instances as $instance_id => $array) {
		if(!isset($insert_strings['control'])) {
			$insert_strings['control'] = "(
				'{$instance_id}', 
				'{$array["source"]}', 
				{$array["amount"]}
			)";
		} else {
			$insert_strings['control'] .= ",(
				'{$instance_id}', 
				'{$array["source"]}', 
				{$array["amount"]}
			)";
		}
	}


	return $insert_strings;
}

function insert_transactions($insert_strings, $table) {
	global $connection;
	$flag = false;
	$errors = array();

	// Don't commit changes unless both queries are successfull
	mysqli_autocommit($connection, false);

	if(isset($insert_strings['control'])) {
		// Insert control query source code data
		$designation_query = "INSERT INTO bog_source_codes (trans_id,source,amount) VALUES {$insert_strings['control']} ";
		$designation_query .= "ON DUPLICATE KEY UPDATE trans_id = trans_id, source = source, amount = amount;";
		$designation_result = mysqli_query($connection, $designation_query);
		if(!$designation_result) {
			$flag = true;
			$errors[] = mysqli_error($connection);
		}
	}
	
	if(isset($insert_strings['trans'])) {
		// Add transactions to DB
		$designation_query = "INSERT INTO {$table} 
		(
			trans_id,
			amount,
			college,
			time,
			name,
			email,
			payment_type,
			reported,
			address,
			member_id,
			allocation,
			match_amount,
			match_code,
			control
		) VALUES ".$insert_strings['trans'].";";
		$designation_result = mysqli_query($connection, $designation_query);
		if(!$designation_result) {
			$flag = true;
			$errors[] = mysqli_error($connection);
		}
	}

	if($flag) {
		mysqli_rollback($connection);
		mysqli_close($connection);
		error_log(json_encode($errors)." on line ".__LINE__);
		die(json_encode($errors));
	} else {
		mysqli_commit($connection);
		return true;
	}
}

function get_start_dates() {
	global $connection;

	// Get Control query results up to 6 hours in the past
	$six_hours_ago = date( 'U', strtotime('6 hours ago') );
	$formatted_six_hours_ago = date('Y-m-d', $six_hours_ago) . 'T' . date('H:i:s', $six_hours_ago);

	// Set start date as beginning of today unless transactions already exist in db
	$start_dates = array(
		'bog' => date('Y-m-d')."T00:00:00",
		'volstarter' => date('Y-m-d')."T00:00:00",
		'sr_impact' => date('Y-m-d')."T00:00:00",
		'bog_control' => $formatted_six_hours_ago
	);

	$select_query = "SELECT control, MAX(time) AS time from bog2018 GROUP BY control;";
	$select_result = mysqli_query($connection, $select_query);
	if(!$select_result) {
		// Save error, close connection, report error
		$error = mysqli_error($connection);
		mysqli_close($connection);
		error_log($error." on line ".__LINE__);
		die($error." on line ".__LINE__);
	}

	while($row = mysqli_fetch_assoc($select_result)) {
		$control = $row['control'];
		$future = strtotime($row['time']) + 1;
		$start_time = date('Y-m-d', $future).'T'.date('H:i:s', $future);
		$start_dates[$control] = $start_time;
	}
	return $start_dates;	
}

function create_query_string($control_id, $start_date, $type) {
	$login = '<username here>';
	$password = '<password here>';
	
	if($type == 'control') {
		// Get columns from txt file if created. Otherwise hit api to get all columns and save to txt file
		if( !$control_column_string = file_get_contents('control_columns.txt') ) {
			$control_column_string = get_control_columns($control_id);
		}

		// try to determine amount column based on column containing 'amt'. Pretty brittle
		$control_column_array = explode(',', $control_column_string);
		foreach($control_column_array as $column_name) {
			if( strpos($column_name, 'amt') !== false ) {
				$amount_column = $column_name;
			}
		}

		$control_query_string = 'https://api.imodules.com/ws/21/ControlQuery.asmx/GetMembersChangedSince';
		$control_query_string .= '?login='.$login;
		$control_query_string .= '&password='.$password;
		$control_query_string .= '&controlId='.$control_id;
		$control_query_string .= '&columns='.$control_column_string;
		$control_query_string .= '&changedColumns=';
		$control_query_string .= '&changedSince='.$start_date;
		$control_query_string .= '&includeNonMembers=true';
		$control_query_string .= '&includeBlankIds=true';
		$control_query_string .= '&includeTransactions=false';
		$control_query_string .= '&includeTransactionDetails=false';
		$control_query_string .= '&includeUnreviewed=true';
		$control_query_string .= '&filter=['.$amount_column.']%3E0';

		return $control_query_string;

	} else {
		$transactions_string  = 'https://api.imodules.com/ws/21/TransactionsQuery.asmx/GetTransactionsByControl';
		$transactions_string .= '?login='.$login;
		$transactions_string .= '&password='.$password;
		$transactions_string .= '&controlId='.$control_id;
		$transactions_string .= '&includeFailedTransactions=false';
		$transactions_string .= '&includeNonMembers=true';
		$transactions_string .= '&includeTransactionDetails=false';
		$transactions_string .= '&includeUnreviewed=true';
		$transactions_string .= '&filter=[purchasedate]%3E%27'.$start_date.'%27';

		return $transactions_string;
	}
}

function get_control_columns($control_id) {
	$login = 'DF64296D-0121-4F2D-AC67-D572BC1C4AEF';
	$password = 'DCH9p84fyfnf4rf';

	$url = 'https://api.imodules.com/ws/21/ControlQuery.asmx/GetAllColumns';
	$url .= '?login='.$login;
	$url .= '&password='.$password;
	$url .= '&controlId='.$control_id;

	$response = curl($url);
	$response_string = json_encode($response);
	$clean_string = str_replace('@attributes', 'attributes', $response_string);
	$response_array = json_decode($clean_string, true);

	if(isset($response_array['AllColumnResults'])) {
		foreach($response_array['AllColumnResults'] as $column_array) {
			$column_name = urlencode( $column_array['attributes']['Column'] );

			if(!isset($columns_string)) {
				$columns_string = $column_name;
			} else {
				$columns_string .= ',' . $column_name;
			}
		}
		file_put_contents('control_columns.txt', $columns_string);
		return $columns_string;
	} else {
		error_log("Coudn't get control columns on line " . __LINE__);
		die("Coudn't get control columns on line " . __LINE__);
	}
}

?>