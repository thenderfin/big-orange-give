<?php
require('../inc/connection.php');
require_once('../inc/functions.php');

// Don't start script before Monday 11/12/18. Stop at end of day Wednesday 11/14/18.
// $golive = date('U', strtotime('2018-11-12 12:00:01 a.m.'));
// $campaign_end = date('U', strtotime('2018-11-15 01:00:00 a.m.'));
$current_time = date('U');
// if($current_time < $golive || $current_time > $campaign_end) {
//     die();
// }

$bog_control_id = '20173';
$volstarter_control_id = '10977';
$sr_impact_control_id = '3108';

$all_transactions = array(
    'bog' => array(),
    'volstarter' => array(),
    'sr_impact' => array()
);

$start_dates = get_start_dates();

$bog_url_string = create_query_string($bog_control_id, $start_dates['bog'], 'transactions');
$volstarter_url_string = create_query_string($volstarter_control_id, $start_dates['volstarter'], 'transactions');
$sr_impact_url_string  = create_query_string($sr_impact_control_id, $start_dates['sr_impact'], 'transactions');
// Attempts to get control columns. If errors, delete control_columns.txt and rerun.
$control_query_string = create_query_string($bog_control_id, $start_dates['bog_control'], 'control');

// Sort bog transactions
$bog_response = curl($bog_url_string);
if(isset($bog_response->Transaction)) {
	// Sort XML into array
    $all_transactions['bog'] = sort_response($bog_response);
}

// Sort volstarter transactions
$volstarter_response = curl($volstarter_url_string);
if(isset($volstarter_response->Transaction)) {
	// Sort XML into array
    $all_transactions['volstarter'] = sort_response($volstarter_response);
}

// Sort sr_impact transactions
$sr_impact_response = curl($sr_impact_url_string);
if(isset($sr_impact_response->Transaction)) {
	// Sort XML into array
    $all_transactions['sr_impact'] = sort_response($sr_impact_response);
}

// Get Control Query Response
$control_query_response = curl($control_query_string);

// Sort Control response into array
$control_response_array = sort_control_response($control_query_response);

// Sort arrays into insert strings and insert in DB
$insert_strings = create_insert_strings ($all_transactions, $control_response_array);
insert_transactions($insert_strings, 'bog2018');

// Close connection
mysqli_close($connection);

?>